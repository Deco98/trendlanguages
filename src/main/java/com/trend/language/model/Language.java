package com.trend.language.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


@Data
public class Language {
    private static AtomicInteger ID_GENERATOR = new AtomicInteger(0000);
    private int id;
    private String name;
    private int numrepos;
    private List<String> repositories;

    public void setRepository(String repository){
        repositories.add(repository);

    }
    public Language() {
        id = ID_GENERATOR.getAndIncrement();
        repositories = new ArrayList<>();
    }
}
