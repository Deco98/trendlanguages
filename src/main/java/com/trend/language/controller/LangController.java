package com.trend.language.controller;

import com.trend.language.model.Language;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping
public class LangController {
    @GetMapping("/trendyLanguages")
    public ResponseEntity<List<Language>> getTrendyLanguages() throws IOException, JSONException, NullPointerException {
        //I choosed a set to store unique elements, also the jsonarray to store data in format json readed from the url
        List<Language> mylanguages  = new ArrayList<>();
        JSONArray jsonArray;
        Set<String> allLanguages = new HashSet<>();

        //here we work to find the -30day from now
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
        Date today = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTime(today);
        cal.add(Calendar.DAY_OF_MONTH, -30);
        Date today30 = cal.getTime();
        String day30= formatter.format(today30);
        System.out.println(day30);

        //here I'm reading th jsondata from the url and then we fetch it to store (in the set) the different languages of dev used in the repos.
        JSONObject json = readJsonFromUrl("https://api.github.com/search/repositories?q=created:%3E"+day30+"&sort=stars&order=desc&per_page=100");
        jsonArray = (JSONArray) json.getJSONArray("items");
        for (int i=0; i < jsonArray.length(); i++) {
            String language = jsonArray.getJSONObject(i).getString("language");
            if(!language.equals("null"))
                allLanguages.add(language);
        }

        //we iterate the set to find the list of repos that correspond to the language name and then we add it to the list of repos without forgetting to increment the numbre of repos
        for (Iterator<String> it = allLanguages.iterator(); it.hasNext(); ) {
            String f = it.next();
            Language l = new Language();
            l.setName(f);
            int num = 0;
            for (int i=0; i < jsonArray.length(); i++) {
                if(f.equals(jsonArray.getJSONObject(i).getString("language"))){
                    l.setRepository(jsonArray.getJSONObject(i).getString("html_url"));
                    num ++;
                }
            }

            l.setNumrepos(num);
            mylanguages.add(l);
        }


        return ResponseEntity.ok(mylanguages);



    }


    //those ar functions that helped me to read the json data from the url
    private String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }
}
